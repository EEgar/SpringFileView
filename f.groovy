@Grab('org.springframework.boot:spring-boot-starter-web')
import org.springframework.boot.*
import org.springframework.boot.autoconfigure.*
import org.springframework.web.bind.annotation.*
import java.io.File
import java.net.URLEncoder

@RestController
@EnableAutoConfiguration
class FileController {

    @GetMapping("/")
    String showCurrentFolder() {
        return showFolder(System.getProperty("user.dir"))
    }

    @GetMapping("/folder")
    String showFolder(@RequestParam(name="path") String path) {
        def folder = new File(path)
        if (!folder.exists() || !folder.isDirectory()) {
            return "Invalid folder path: ${path}"
        }
        def files = folder.listFiles()
        def html = """
        <html>
        <head>
            <title>${folder.getName()}</title>
        </head>
        <body>
            <h1>${folder.getName()}</h1>
            <ul>
        """
        files.each { file ->
            def name = file.getName()
            def encodedPath = URLEncoder.encode(file.getAbsolutePath(), "UTF-8")
            def link = file.isDirectory() ? "/folder?path=${encodedPath}" : "/file?path=${encodedPath}"
            html += "<li><a href='${link}'>$name</a></li>"
        }
        html += """
            </ul>
        </body>
        </html>
        """
        return html
    }

    @GetMapping("/file")
    String showFile(@RequestParam(name="path") String path) {
        def file = new File(path)
        if (!file.exists() || !file.isFile()) {
            return "Invalid file path: ${path}"
        }
        def html = """
        <html>
        <head>
            <title>${file.getName()}</title>
        </head>
        <body>
            <h1>${file.getName()}</h1>
            <pre>
        """
        html += file.text
        html += """
            </pre>
        </body>
        </html>
        """
        return html
    }
}

class Application {
    static void main(String[] args) {
        SpringApplication.run(FileController, args)
    }
}

Application.main(args)
